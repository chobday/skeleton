import React, { Component } from 'react'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'

console.log(JSON.stringify(process.env, null, 2))
const API = `http://${process.env.REACT_APP_SERVER}:${process.env.REACT_APP_SERVER_PORT}/api/health` //'http://localhost:3000/api/health' //TODO put under api


class ServerStatus extends Component {
    constructor(props) {
        super(props)
        this.state = {
          serverStartTime: null,
          lastSuccess : "None",
        }
      }
    
      async pollServer() {
        try {
          const data = await fetch(API)
          const msg = await data.json()
          this.setState({
            serverStartTime: new Date(msg.serverStartTime),
            lastSuccess: new Date().toTimeString()
          })
    
        } catch(err) {
          console.log(err)
          this.setState({
            serverStartTime: null,
          });
        }
      }
    
      componentDidMount() {
        const TEN_SECONDS = 1000 * 10
        this.timerID = setInterval(
          () => this.pollServer(),
          TEN_SECONDS
        );
      }
    
      componentWillUnmount() {
        clearInterval(this.timerID);
      }

  getTimeDiff() {
    const timeDiffMs =  new Date() - this.state.serverStartTime

    const ONE_SECOND = 1000
    const ONE_MINUTE = 1000 * 60
    const ONE_HOUR = 1000 * 60 * 60
    const ONE_DAY = 1000 * 60 * 60 * 24


    let timeDiffText = ''
    if (timeDiffMs < ONE_SECOND) {
      timeDiffText = `${timeDiffMs} milliseconds`
    } else if (timeDiffMs < ONE_MINUTE) {
      timeDiffText = `${Math.floor( timeDiffMs / ONE_SECOND)} seconds`
    } else if (timeDiffMs < ONE_HOUR ) {
      timeDiffText = `${Math.floor( timeDiffMs / ONE_MINUTE)} minutes`
    } else if (timeDiffMs < ONE_DAY ) {
      timeDiffText = `${Math.floor( timeDiffMs/ ONE_HOUR)} hours`
    } else {
      timeDiffText = `${Math.floor( timeDiffMs/ ONE_DAY)} days`
    }
    return timeDiffText
  }



  render() {
    const timeDiff = this.getTimeDiff()

    
    const cardStyle = {
        height: '20vw'
    }
    const cardHeaderStyle = {
        'backgroundColor': 'rgba(0,0,0,.03)',
        'borderBottom': '1px solid rgba(0,0,0,.125)'
    }
    const serverUpJsx = (this.state.serverStartTime &&
        <Card style={cardStyle}>  
            <CardHeader title="Healthy"
                style={cardHeaderStyle}
            />
            <CardContent>
                <Typography variant="subtitle1">
                    Server has been up for {timeDiff}
                </Typography>
                <Typography variant="subtitle1">
                    Server start time: {this.state.serverStartTime.toTimeString()}
                </Typography>
            </CardContent>
        </Card>
    )
    const serverDownJsx = (
        <Card style={cardStyle}>  
            <CardHeader title="Unhealthy" style={cardHeaderStyle} />
            <CardContent>
                <Typography variant="subtitle1">
                    Cannot run health check to server!
                </Typography>
                <Typography variant="subtitle1">
                Last successful health check: {this.state.lastSuccess}
                </Typography>
            </CardContent>
        </Card>
    )

    return ( <div>
        {this.state.serverStartTime ? serverUpJsx : serverDownJsx}
        </div>
    )
  }
}

export default ServerStatus;
