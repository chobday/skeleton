#!/bin/bash

# any future command that fails will exit the script
set -ev

PROJECT=skeleton

# Lets write the public key of our aws instance
eval $(ssh-agent -s)
echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

# disable the host key checking.
mkdir -p ~/.ssh
touch ~/.ssh/config
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config

rm -rf node_modules #copying takes a long time

DEPLOY_SERVERS=$DEPLOY_SERVERS
ALL_SERVERS=(${DEPLOY_SERVERS//,/ })
echo "ALL_SERVERS ${ALL_SERVERS}"
# sed -i 's/\r$//' ./updateAndRestart.sh
for server in "${ALL_SERVERS[@]}"
do
  echo "deploying to ${server}"
  scp -r ../${PROJECT} ubuntu@${server}:deploy
  ssh ubuntu@${server} 'bash' < ./updateAndRestart.sh 
done