import express from 'express'
// import Intl from 'intl'
import Cors from 'cors'
import { runInNewContext } from 'vm'

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config()
}

const app = express()

//TODO whitelist
app.use(Cors())
app.use(express.static('public'))

const router = express.Router()
app.use('/api', router)

const port = process.env.SERVER_PORT

const options = { year: '2-digit', month: '2-digit', day: 'numeric',
                hour: 'numeric', minute: '2-digit', second: '2-digit', hour12: true,
                timeZoneName: 'short' };
const britishDateTime = new Intl.DateTimeFormat('en-GB', options).format;

const startTime = new Date()

router.use((req, res, next) => {
    const currentTime = britishDateTime(Date.now())
    console.log(`${currentTime}: Message received @ ${req.originalUrl}`) //TODO add logging to file
    next()
})

router.route('/health').get((req, res) => {
    res.json({'serverStartTime': startTime } )
})

app.listen(port, () => console.log(`Listening on port ${port}!`))