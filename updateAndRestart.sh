#!/bin/bash 

source /home/ubuntu/.nvm/nvm.sh

set -ev
PROJECT=skeleton

rm -rf /home/ubuntu/${PROJECT}/
mv /home/ubuntu/deploy/${PROJECT} /home/ubuntu/${PROJECT}

cd /home/ubuntu/${PROJECT}/server-app
npm install

pm2 restart server
pm2 status
